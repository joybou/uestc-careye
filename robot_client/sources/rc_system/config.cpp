//
// Created by pulsarv on 21-1-29.
//
#include <opencv2/opencv.hpp>
#include <rc_system/config.h>
#include <rc_log/slog.hpp>
#include <utility>

namespace rccore {
    namespace common {
        bool Config::generate_config() {
            slog::warn << "Generate Default Config File!" << slog::endl;
            cv::FileStorage fswrite(m_config_file_path, cv::FileStorage::WRITE);
            fswrite.writeComment("Default Config File", true);


            fswrite << "CameraSettings"
                    << "{";
            fswrite.write("CAMERA_INDEX", pconfigInfo->CAMERA_INDEX);
            fswrite.writeComment("<摄像头下标>", true);
            fswrite.write("CAMERA_INFO", pconfigInfo->CAMERA_INFO);
            fswrite.writeComment("<摄像头标定数据路径>", true);
            fswrite.write("RESIZE_WIDTH", pconfigInfo->RESIZE_WIDTH);
            fswrite.writeComment("<图像宽度>", true);
            fswrite.write("RESIZE_HEIGHT", pconfigInfo->RESIZE_HEIGHT);
            fswrite.writeComment("<图像高度>", true);
            fswrite.write("CAMERA_DETCET_SIZE", pconfigInfo->CAMERA_DETCET_SIZE);
            fswrite.writeComment("<识别缩放倍数>", true);
            fswrite.write("CAMERA_FPS", pconfigInfo->CAMERA_FPS);
            fswrite.writeComment("摄像头FPS", true);
            fswrite << "}";


            fswrite << "LidarSettings"
                    << "{";
            fswrite.write("LIDAR_DEVICE_PATH", pconfigInfo->LIDAR_DEVICE_PATH);
            fswrite.writeComment("<激光雷达串口地址>", true);
            fswrite << "}";

            fswrite << "GyroSettings"
                    << "{";
            fswrite.write("GYRO_DEVICE_PATH", pconfigInfo->GYRO_DEVICE_PATH);
            fswrite.writeComment("<IMU串口地址>", true);
            fswrite.write("GYRO_DEVICE_FREQ", pconfigInfo->GYRO_DEVICE_FREQ);
            fswrite.writeComment("<IMU串口频率>", true);
            fswrite.write("GYRO_DEVICE_HEAD", pconfigInfo->GYRO_DEVICE_HEAD);
            fswrite.writeComment("<IMU串口频率>", true);
            fswrite.write("GYRO_DEVICE_DATA_SIZE", pconfigInfo->GYRO_DEVICE_DATA_SIZE);
            fswrite.writeComment("<IMU串口数据包大小>", true);
            fswrite << "}";

            fswrite << "MovingControlSettings"
                    << "{";
            fswrite.write("USE_MRAA", pconfigInfo->USE_MRAA);
            fswrite.writeComment("<使能MRAA>", true);

            fswrite.write("GPIO_1", pconfigInfo->GPIO_1);
            fswrite.writeComment("<1号轮方向控制端口>", true);
            fswrite.write("PWM_1", pconfigInfo->PWM_1);
            fswrite.writeComment("<1号轮速度控制端口>", true);

            fswrite.write("GPIO_2", pconfigInfo->GPIO_2);
            fswrite.writeComment("<2号轮方向控制端口>", true);
            fswrite.write("PWM_2", pconfigInfo->PWM_2);
            fswrite.writeComment("<2号轮速度控制端口>", true);

            fswrite.write("GPIO_3", pconfigInfo->GPIO_3);
            fswrite.writeComment("<3号轮方向控制端口>", true);
            fswrite.write("PWM_3", pconfigInfo->PWM_3);
            fswrite.writeComment("<3号轮速度控制端口>", true);

            fswrite.write("DSP_DEVICE_SERIAL_PATH", pconfigInfo->DSP_DEVICE_SERIAL_PATH);
            fswrite.writeComment("<3号轮速度控制端口>", true);
            fswrite.write("DSP_DEVICE_FREQ", pconfigInfo->DSP_DEVICE_FREQ);
            fswrite.writeComment("<下位控制器串口频率>", true);

            fswrite << "}";

            fswrite << "NetWorkSettings"
                    << "{";
            fswrite.write("HTTPD_ON", pconfigInfo->HTTPD_ON);
            fswrite.writeComment("<是否打开网络服务模块>", true);
            fswrite.write("HTTPD_PORT", pconfigInfo->HTTPD_PORT);
            fswrite.writeComment("<是否打开网络服务模块>", true);
            fswrite.write("REMOTE_SERVER_IPADDRESS", pconfigInfo->REMOTE_SERVER_IPADDRESS);
            fswrite.writeComment("<远程服务器IP>", true);
            fswrite.write("REMOTE_SERVER_PORT", pconfigInfo->REMOTE_SERVER_PORT);
            fswrite.writeComment("<远程服务器端口>", true);
            fswrite << "}";

            fswrite << "ControllerSettings"
                    << "{";
            fswrite.write("DELTA_T", pconfigInfo->DELTA_T);
            fswrite.writeComment("<积分时间>", true);
            fswrite.write("transitionMatrix", pconfigInfo->transitionMatrix);
            fswrite.writeComment("<状态转移矩阵>", true);
            fswrite.write("MAX_POINTS_SIZE", pconfigInfo->MAX_POINTS_SIZE);
            fswrite.writeComment("<最大目标点缓存队列大小>", true);
            fswrite.write("EKF_ON", pconfigInfo->EKF_ON);
            fswrite.writeComment("<开启卡尔曼滤波器>", true);


            fswrite.write("WHEEL_1_ON", pconfigInfo->WHEEL_1_ON);
            fswrite.writeComment("<1号轮开关>", true);
            fswrite.write("WHEEL_1_MAX_V_M_S", pconfigInfo->WHEEL_1_MAX_V_M_S);
            fswrite.writeComment("<1号轮最大速度>", true);
            fswrite.write("WHEEL_1_P", pconfigInfo->WHEEL_1_P);
            fswrite.writeComment("<1号轮PID参数P>", true);
            fswrite.write("WHEEL_1_I", pconfigInfo->WHEEL_1_I);
            fswrite.writeComment("<1号轮PID参数I>", true);
            fswrite.write("WHEEL_1_D", pconfigInfo->WHEEL_1_D);
            fswrite.writeComment("<1号轮PID参数D>", true);

            fswrite.write("WHEEL_2_ON", pconfigInfo->WHEEL_2_ON);
            fswrite.writeComment("<2号轮开关>", true);
            fswrite.write("WHEEL_2_MAX_V_M_S", pconfigInfo->WHEEL_2_MAX_V_M_S);
            fswrite.writeComment("<2号轮最大速度>", true);
            fswrite.write("WHEEL_2_P", pconfigInfo->WHEEL_2_P);
            fswrite.writeComment("<2号轮PID参数P>", true);
            fswrite.write("WHEEL_2_I", pconfigInfo->WHEEL_2_I);
            fswrite.writeComment("<2号轮PID参数I>", true);
            fswrite.write("WHEEL_2_D", pconfigInfo->WHEEL_2_D);
            fswrite.writeComment("<2号轮PID参数D>", true);

            fswrite.write("WHEEL_3_ON", pconfigInfo->WHEEL_3_ON);
            fswrite.writeComment("<3号轮开关>", true);
            fswrite.write("WHEEL_3_MAX_V_M_S", pconfigInfo->WHEEL_3_MAX_V_M_S);
            fswrite.writeComment("<3号轮最大速度>", true);
            fswrite.write("WHEEL_3_P", pconfigInfo->WHEEL_3_P);
            fswrite.writeComment("<3号轮PID参数P>", true);
            fswrite.write("WHEEL_3_I", pconfigInfo->WHEEL_3_I);
            fswrite.writeComment("<3号轮PID参数I>", true);
            fswrite.write("WHEEL_3_D", pconfigInfo->WHEEL_3_D);
            fswrite.writeComment("<3号轮PID参数D>", true);
            fswrite << "}";

            fswrite << "SystemSettings"
                    << "{";
            fswrite.write("DEBUG_WINDOW", pconfigInfo->DEBUG_WINDOW);
            fswrite.writeComment("<是否打开可视化窗口>", true);
            fswrite.write("AUTO_CONTROL", pconfigInfo->AUTO_CONTROL);
            fswrite.writeComment("<是否打开自动控制>", true);
            fswrite.write("LOSS_HOVER_TIMEOUT_TIME", pconfigInfo->LOSS_HOVER_TIMEOUT_TIME);
            fswrite.writeComment("<目标丢失超时>", true);
            fswrite.write("USE_DETCET_PLUGUNS", pconfigInfo->USE_DETCET_PLUGUNS);
            fswrite.writeComment("<使用插件>", true);
            fswrite.write("DETCET_PLUGUNS_PATH", pconfigInfo->DETCET_PLUGUNS_PATH);
            fswrite.writeComment("<插件动态链接库路径>", true);
            fswrite << "}";

            fswrite << "MessageSettings"
                    << "{";
            fswrite.write("HAVE_DSP_SERIAL_MESSAGE", pconfigInfo->HAVE_DSP_SERIAL_MESSAGE);
            fswrite.writeComment("<下位控制器串口消息>", true);
            fswrite.write("DSP_SERIAL_MESSAGE_QUEUE_SIZE", pconfigInfo->HAVE_DSP_SERIAL_MESSAGE);
            fswrite.writeComment("<下位控制器串口消息队列大小>", true);

            fswrite.write("HAVE_NETWORK_MESSAGE", pconfigInfo->HAVE_NETWORK_MESSAGE);
            fswrite.writeComment("<网络服务消息>", true);
            fswrite.write("NETWORK_MESSAGE_QUEUE_SIZE", pconfigInfo->NETWORK_MESSAGE_QUEUE_SIZE);
            fswrite.writeComment("<网络服务消息队列大小>", true);

            fswrite.write("HAVE_IMAGE_MESSAGE", pconfigInfo->HAVE_IMAGE_MESSAGE);
            fswrite.writeComment("<图像消息>", true);
            fswrite.write("IMAGE_MESSAGE_QUEUE_SIZE", pconfigInfo->IMAGE_MESSAGE_QUEUE_SIZE);
            fswrite.writeComment("<图像消息队列大小>", true);

            fswrite.write("HAVE_DEPTH_IMAGE_MESSAGE", pconfigInfo->HAVE_DEPTH_IMAGE_MESSAGE);
            fswrite.writeComment("<深度图像消息>", true);
            fswrite.write("DEPTH_IMAGE_MESSAGE_QUEUE_SIZE", pconfigInfo->DEPTH_IMAGE_MESSAGE_QUEUE_SIZE);
            fswrite.writeComment("<深度图像消息队列大小>", true);

            fswrite.write("HAVE_MOVE_MESSAGE", pconfigInfo->HAVE_MOVE_MESSAGE);
            fswrite.writeComment("<底盘控制消息>", true);
            fswrite.write("MOVE_MESSAGE_QUEUE_SIZE", pconfigInfo->MOVE_MESSAGE_QUEUE_SIZE);
            fswrite.writeComment("<底盘控制消息队列大小>", true);

            fswrite.write("HAVE_RADAR_MESSAGE", pconfigInfo->HAVE_RADAR_MESSAGE);
            fswrite.writeComment("<雷达消息>", true);
            fswrite.write("RADAR_MESSAGE_QUEUE_SIZE", pconfigInfo->RADAR_MESSAGE_QUEUE_SIZE);
            fswrite.writeComment("<雷达消息队列大小>", true);

            fswrite.write("HAVE_IMU_MESSAGE", pconfigInfo->HAVE_IMU_MESSAGE);
            fswrite.writeComment("<IMU消息>", true);
            fswrite.write("IMU_MESSAGE_QUEUE_SIZE", pconfigInfo->IMU_MESSAGE_QUEUE_SIZE);
            fswrite.writeComment("<IMU消息队列大小>", true);

            fswrite << "}";

            fswrite.release();
            return true;
        }

        Config::Config(std::string config_file_path) : m_config_file_path(std::move(config_file_path)) {
            pconfigInfo.reset(new data_struct::ConfigInfo);
        }

        Config::~Config() = default;

        Config::Config() = default;


    } // namespace config
} // namespace follow_down
