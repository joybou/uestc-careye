//
// Created by pulsarv on 21-2-4.
//
#ifndef RAINBOW_CAMERA_CALIBRATION_FUNCTIONS_H
#define RAINBOW_CAMERA_CALIBRATION_FUNCTIONS_H

#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <rc_log/slog.hpp>

namespace rccore {
    namespace functions {
        class Calibration {
        public:
            Calibration(size_t aqXnum,
                        size_t aqYnum,
                        size_t width,
                        size_t height,
                        std::string image_path,
                        const std::string &output_xml_filename
            );

            void next();

            bool is_end();

            void caclulate();

            void bind_show(std::function<void(cv::Mat)> view_function);

            static bool createOutputDir(const std::string &outputPath) ;

        private:
            size_t aqXnum{};
            size_t aqYnum{};
            size_t width{};
            size_t height{};
            std::string image_path;
            std::string output_xml_filename;
            std::vector<cv::Point2f> corner_pts;
            size_t current_image_index = 0;
            std::vector<cv::String> images;
            std::vector<cv::Point3f> objp;
            std::vector<std::vector<cv::Point3f>> objpoints;
            std::vector<std::vector<cv::Point2f>> imgpoints;
            cv::Mat current_image;
            std::function<void(cv::Mat)> m_view_function;
            int IMAGE_WIDTH;
            int IMAGE_HEIGHT;
        };

    }
}

#endif