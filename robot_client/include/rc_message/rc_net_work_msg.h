//
// Created by Pulsar on 2020/5/6.
//

#ifndef ROBOCAR_RC_NET_WORK_MSG_H
#define ROBOCAR_RC_NET_WORK_MSG_H

#include <rc_message/rc_base_msg.hpp>
#include <map>
#include <rc_network/rc_asny_tcp_client.h>

namespace rccore {
    namespace message {
        class NetworkMessage : public BaseMessage<std::map<int, std::string>> {
        public:
            explicit NetworkMessage(int max_queue_size);
        };
    }
}


#endif //ROBOCAR_CAREYE_RC_NET_WORK_MSG_H
