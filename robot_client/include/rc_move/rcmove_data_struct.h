//
// Created by PulsarV on 18-5-9.
//

#ifndef ROBOCAR_RCMOVE_DATA_STRUCT_H
#define ROBOCAR_RCMOVE_DATA_STRUCT_H
//车轮控制数据
struct WHEEL_DATA {
    float wheel_1_v_m_s = 0.0f;
    float wheel_2_v_m_s = 0.0f;
    float wheel_3_v_m_s = 0.0f;
    float wheel_4_v_m_s = 0.0f;
    float wheel_1_a_v_m_s = 0.0f;
    float wheel_2_a_v_m_s = 0.0f;
    float wheel_3_a_v_m_s = 0.0f;
    float wheel_4_a_v_m_s = 0.0f;
    bool weel_1_direction = false;
    bool weel_2_direction = false;
    bool weel_3_direction = false;
    bool weel_4_direction = false;
};

enum {
    RC_MOVE_BACKWARD = 0,
    RC_MOVE_FORWARD
};
#endif //ROBOCAR_RCMOVE_DATA_STRUCT_H
