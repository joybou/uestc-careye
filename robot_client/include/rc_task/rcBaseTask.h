//
// Created by Pulsar on 2021/8/23.
//

#ifndef ROBOCAR_RCBASETASK_H
#define ROBOCAR_RCBASETASK_H

#include <condition_variable>
#include <mutex>
#include <atomic>
#include <thread>
#include <memory>
#include <rc_system/context.h>

namespace rccore {
    namespace task {
        class BaseTask {
        protected:
            bool m_isPause = false;// 线程暂停FLAG
            std::atomic<bool> m_isStop = false;// 线程终止FLAG
            std::condition_variable m_cv; // FLAG访问互斥变量
            std::mutex m_mutex; // 互斥锁
            std::shared_ptr<common::Context> pcontext; // 全局上下文
            std::thread *m_thread = nullptr; // 线程

        public:
            /**
             * 基础线程类
             * @param _pcontext 全局上下文
             */
            explicit BaseTask(std::shared_ptr<common::Context> _pcontext);

            ~BaseTask();

            /**
             * 停止运行
             */
            virtual void Stop();

            /**
             * 暂停运行
             */
            void Pause();

            /**
             * 恢复运行
             */
            void Resume();

            /**
             * 开始运行
             */
            virtual void Run() {};

            /**
             * 阻塞等待
             */
            void Join();

            /**
             * 释放阻塞
             */
            void Detach();
        };

    }
}

#endif //ROBOCAR_RCBASETASK_H
