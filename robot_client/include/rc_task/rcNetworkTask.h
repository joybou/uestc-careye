//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCNETWORKTASK_H
#define UESTC_CAREYE_RCNETWORKTASK_H

#include <rc_system/context.h>
#include <rc_system/data_struct.h>
#include <rc_task/rcBaseTask.h>

namespace rccore {
    namespace task {
        class NetworkTask : public BaseTask {
        public:
            explicit NetworkTask(std::shared_ptr<common::Context> pcontext);

            void Run();

            ~NetworkTask();
        };

        /**
         * 启动网络服务
         * @param device_info
         */
        void run_network_task(std::shared_ptr<common::Context> pcontext);
    }
}


#endif //UESTC_CAREYE_RCNETWORKTASK_H
