// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCCVTASK_H
#define UESTC_CAREYE_RCCVTASK_H

#include "rc_task/rcBaseTask.h"

namespace rccore {
    namespace task {
        class CvTask : public BaseTask {
        public:
            explicit CvTask(std::shared_ptr<common::Context> pcontext);

            void Run();

            ~CvTask();
        };
    }
}


#endif //UESTC_CAREYE_RCCVTASK_H
