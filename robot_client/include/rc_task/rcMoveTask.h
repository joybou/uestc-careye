//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCMOVETASK_H
#define UESTC_CAREYE_RCMOVETASK_H

#include <rc_move/rcmove.h>

namespace rccore {
    namespace task {
        class MoveTask : public move::RobotCarMove {
        public:
            explicit MoveTask(std::shared_ptr<common::Context> pcontext);

            void Run();

            void Stop();

            ~MoveTask();
        };
    }
}


#endif //UESTC_CAREYE_RCMOVETASK_H
