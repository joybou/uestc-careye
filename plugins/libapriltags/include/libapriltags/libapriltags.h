//
// Created by Pulsar on 2021/8/26.
//

#ifndef ROBOCAR_HUMANDETECTOR_H
#define ROBOCAR_HUMANDETECTOR_H

#include <opencv2/opencv.hpp>
#include <rc_system/data_struct.h>
#include <rc_message/rc_move_msg.h>

extern "C" {
void *InitCallback();
void *BeforeDetcetCallback(void *args);
void DetectCallback(cv::Mat &frame, void *args, const std::shared_ptr<rccore::message::MoveMessage> &p_move_message);
void *AfterDetcetCallback(void *args);
void OnThreadBreakCallback(void *args);
};

#endif //ROBOCAR_HUMANDETECTOR_H
