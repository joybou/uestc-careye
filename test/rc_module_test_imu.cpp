//
// Created by Pulsar on 2020/5/16.
//
#include <iostream>
#include <rc_system/config.h>
#include <rc_system/context.h>
#include <signal.h>
#include <rc_task/rcTaskVariable.h>
#include <rc_task/rcMoveTask.h>
#include <rc_task/rcGyroTask.h>
#include <rc_log/slog.hpp>

volatile sig_atomic_t flag = 1;

void sig_handler(int signum) {
    if (signum == SIGINT) {
        std::cout << "Exiting..." << std::endl;
        flag = 0;
    }
}

int main(int argc, char **argv) {
    signal(SIGINT, sig_handler);
    std::shared_ptr<rccore::common::Config> pconfig = std::make_shared<rccore::common::Config>("config.yml");
    if (pconfig->Load())
        pconfig->Load();
    std::shared_ptr<rccore::common::Context> pcontext = std::make_shared<rccore::common::Context>(pconfig);
    pcontext->Init();
    rccore::task::GyroTask imu_task(pcontext);
    imu_task.Run();
    while (flag) {}
    imu_task.Stop();
    while (rccore::task::task_variable::TASK_NUM) {};
    return 1;
}