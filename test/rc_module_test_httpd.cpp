//
// Created by Pulsar on 2020/5/16.
//
#include <rc_system/config.h>
#include <rc_system/context.h>
#include <rc_task/rcCVTask.h>
#include <rc_task/rcWebStream.h>
#include <rc_task/rcMoveTask.h>
#include <rc_task/rcGyroTask.h>
#include<atomic>

std::atomic<bool> flag = true;

void sig_handler(int signum) {
    if (signum == SIGINT) {
        if (flag) {
            std::cout << "Exiting..." << std::endl;
            flag = false;
        } else {
            exit(0);
        }
    }
}

//机器人联合控制测试
int main(int argc, char **argv) {
    signal(SIGINT, sig_handler);
    std::shared_ptr<rccore::common::Config> pconfig = std::make_shared<rccore::common::Config>("config.yml");
    if (pconfig->Load())
        pconfig->Load();
    std::shared_ptr<rccore::common::Context> pcontext = std::make_shared<rccore::common::Context>(pconfig);
    pcontext->Init();
//    rccore::task::CvTask cv_task(pcontext);
    rccore::task::WebStreamTask webstream_task(pcontext);
//    rccore::task::GyroTask imu_task(pcontext);
//    rccore::task::MoveTask move_task(pcontext);
//    move_task.Run();
//    imu_task.Run();
//    cv_task.Run();
    webstream_task.Run();
    while (flag) {}
//    move_task.Stop();
//    cv_task.Stop();
//    imu_task.Stop();
    webstream_task.Stop();
    while (rccore::task::task_variable::TASK_NUM) {};
    return 1;
}