project(robocar_test)
message(STATUS ===================================RoboCarTest============================)

set(TEST_SOURCES_DIR ${RC_SOURCE_DIR}/test)
if (${WITH_LIBREALSENCE2} STREQUAL "ON")
    add_definitions(-D WITH_LIBREALSENCE2)
endif ()
include_directories(${RC_CLIENT_INCLUDE_DIR}/common)
include_directories(${PROJECT_BINARY_DIR})
include_directories(${RC_CLIENT_INCLUDE_DIR})
include_directories(${PROJECT_SOURCE_DIR}/../3rdparty/pistache/include)
include_directories(${PROJECT_SOURCE_DIR}/../3rdparty/libserv/src/include)

include(${ENV_CMAKE_FILES_PATH}/FindMRAACmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcPlathformConfigCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcBoostCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcOpenGLCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcOpenCVCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcPistacheCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcLibrealsense2Cmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcProtobufCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcMpiCmake.cmake)
include(${ENV_CMAKE_FILES_PATH}/rcLibservCmake.cmake)

find_package(PkgConfig REQUIRED)

pkg_check_modules(GTK3 REQUIRED gtk+-2.0)
include_directories(${GTK2_INCLUDE_DIRS})
link_directories(${GTK2_LIBRARY_DIRS})

pkg_check_modules(GTKMM2 gtkmm-2.4)
link_directories(${GTKMM2_LIBRARY_DIRS})
include_directories(${GTKMM3_INCLUDE_DIRS})

pkg_check_modules(GLEW glew2.0)
link_directories(${GLEW_LIBRARY_DIRS})
include_directories(${GLEW_INCLUDE_DIRS})
if (RC_TEST)
    file(GLOB TEST_SOURCES_FILES ${TEST_SOURCES_DIR}/*.cpp)
    foreach (TEST_EXE ${TEST_SOURCES_FILES})
        get_filename_component(mainname ${TEST_EXE} NAME_WE)
        message(STATUS "TEST_EXE:${mainname}")

        add_executable(${mainname} ${TEST_EXE})
        target_link_libraries(${mainname}
                ${RC_THREAD_LIBS}
                #                GLEW
                GLU
                rccore
                #                gtkgl-2.0
                )
    endforeach ()
endif ()

message(STATUS ===================================RoboCarTestDone============================)
